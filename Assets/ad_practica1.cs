﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using SQLite4Unity3d;
using System.Collections.Generic;

public class ad_practica1 : MonoBehaviour {
	

	private static string databasefile = "database";

	// Variables sqlite
	private SQLiteConnection _connection;

	// Use this for initialization
	void Start () {

		// Miramos si existe nuestra BD en persistend data base
		string filepath = Application.persistentDataPath +"/"+ databasefile;
		if (!File.Exists (filepath)) {
			//Copiamos la base de datos de streamingAssets a persistendDatabase
			#if UNITY_ANDROID 
				WWW loadDb = new WWW(Application.streamingAssetsPath +"/"+ databasefile);  // this is the path to your StreamingAssets in android
				while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
				// then save to Application.persistentDataPath
				File.WriteAllBytes(filepath, loadDb.bytes);
			#else
				string appdb = Application.streamingAssetsPath +"/"+ databasefile;  // this is the path to your StreamingAssets in iOS
				// then save to Application.persistentDataPath
				File.Copy(appdb, filepath);
			#endif
		}

		//Ya tenemos nuestra bd en su url
		Debug.Log ("DB in : " + filepath);

		_connection = new SQLiteConnection(filepath, SQLiteOpenFlags.ReadWrite);

		readDB1 ();

		readDB2 ();

		writeUser (randomNick(),randomHighscore());

		readDB1 ();
	}

	void readDB1 ()
	{
		Debug.Log ("readDB1");
		string query = "SELECT * FROM usersalternative";

		var highscores = _connection.Query<usersalternative> (query);

		foreach (usersalternative user in highscores) // Loop through List with foreach.
		{
			Debug.Log("Usuario : "+user.name +" highscore : "+user.highscore);
		}
	}

	void readDB2 ()
	{
		Debug.Log ("readDB2");
		var highscores = _connection.Table<usersalternative> ().OrderBy( x => x.highscore);
		
		foreach (usersalternative user in highscores) // Loop through List with foreach.
		{
			Debug.Log("Usuario : "+user.name +" highscore : "+user.highscore);
		}
	}

	void writeUser (string user, int highscore)
	{
		usersalternative ua = new usersalternative();
		ua.name = user;
		ua.highscore = highscore;

		_connection.Insert(ua);
	}
	
	string randomNick(){
		var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		var random = new System.Random();
		var stringChars = new char[random.Next(8,12)];


		for (int i = 0; i < stringChars.Length; i++)
		{
			stringChars[i] = chars[random.Next(chars.Length)];
		}
		
		return new String(stringChars);
	}

	int randomHighscore(){
		var random = new System.Random();
		return random.Next(0,10000);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
